import cv2
import numpy as np

from image_manipulation import contour_Centroids, draw_Rectangle, remove_Small_Object, threshold, detect_Line_Object
from image_tools import show_image, save_image
from stdo import stdo

def preprocess_of_Detect_Stain(
    image,
    is_middle_object=False, 
    middle_object_roi=[],
    threshold_config=[-1, -1],
    is_line=False,
    line_frame_preprocessed=None,
    is_stain_threshold=False, 
    stain_threshold_config=[-1, -1],
    crop_ratio=5,  
    sector_index='',
    background_color='white', 
    activate_debug_images=False
):
    """ 
    if (len(image.shape) == 3):
        gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    """
    
    #
    if is_middle_object:
        mask_image = image.copy()
        if background_color == 'white' or background_color == 'gray':
            mask_image[middle_object_roi[0]:middle_object_roi[1], middle_object_roi[2]:middle_object_roi[3]] = 255, 255, 255
        elif background_color == 'black':
            mask_image[middle_object_roi[0]:middle_object_roi[1], middle_object_roi[2]:middle_object_roi[3]] = 0, 0, 0
    else:
        mask_image = image
    
    #
    if background_color == 'white' or background_color == 'gray':
        color = (255,255,255)
    elif background_color == 'black':
        color = (0,0,0)
    temp_mask_image = mask_image.copy()
    fill_padded_image = draw_Rectangle(
        temp_mask_image, 
        start_point=(0,0), 
        end_point=(mask_image.shape[1], mask_image.shape[0]), 
        color=color, 
        thickness=20
    )
    
    #
    if is_line:
        # gray_image_fill_padded_image = cv2.cvtColor(fill_padded_image, cv2.COLOR_RGB2GRAY)
        # image_threshold = threshold(gray_image_fill_padded_image.copy(), configs=threshold_config)
        if background_color == 'white' or background_color == 'gray':
            padded_line_frame_preprocessed = cv2.copyMakeBorder(line_frame_preprocessed, 10,10,10,10, cv2.BORDER_CONSTANT, None, value=[0])
        elif background_color == 'black':
            padded_line_frame_preprocessed = cv2.copyMakeBorder(line_frame_preprocessed, 10,10,10,10, cv2.BORDER_CONSTANT, None, value=[0])
            
        _, _, draw_masking_image  = detect_Line_Object(
            line_eliminate_image=padded_line_frame_preprocessed.copy(),
            draw_masking_image=fill_padded_image.copy(),
            background_color=background_color,
            flag_activate_debug_images=activate_debug_images
        )
    else:
        draw_masking_image = fill_padded_image.copy()
    
    #
    gray_image = cv2.cvtColor(draw_masking_image, cv2.COLOR_RGB2GRAY)
    if is_stain_threshold:
        image_threshold = threshold(gray_image.copy(), configs=stain_threshold_config)
    else:
        image_threshold = gray_image
    
    
    if activate_debug_images:
        list_temp_save_image = [image, mask_image, fill_padded_image, draw_masking_image, image_threshold]
        filename = [str(sector_index)+"_1_image", str(sector_index)+"_2_mask_image", str(sector_index)+"_3_fill_padded_image", str(sector_index)+"_4_draw_masking_image", str(sector_index)+"_5_image_threshold"]
        save_image(list_temp_save_image, path="temp_files/detect_stains/preprocess_of_Detect_Stain", filename=filename, format="png")
    
    return image_threshold
    
    
        
    