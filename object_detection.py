import os
import time

import cv2
import numpy as np
import torch

import libs
from image_manipulation import draw_Rectangle, draw_Text
from image_tools import save_image
from stdo import stdo
from math_tools import coordinate_Scaling


def detect_Fiducial_Marker(image, process_methods=None, configuration_id='N/A', pattern_id='0', counter=0, flag_activate_debug_images=False):
    
    start_time = time.time()
    predicted_class = None
    decision = False
    result_dashboard = "Fiducial Marker:{} - Pred:[N/A] - T:{}ms".format(decision, 0)
    
    dict_counter_fiducial_class = {
        "circle":1,
        "plus":1,
        "square":1
    }
    
    temp_image = image.copy()
    dict_predicted_results = dict()
    prediction = process_methods.predict(source=image, verbose=False)[0]
    
    for id, cls in enumerate(prediction.boxes.cls):
        class_names = prediction.names
        predicted_class = class_names[int(cls)]
        predicted_prob = np.round(float(prediction.boxes.conf[id]), 2)
        predicted_bbox = list(map(int, prediction.boxes.xyxy[id].tolist()))
        center_x = predicted_bbox[0] + ((predicted_bbox[2] - predicted_bbox[0]) // 2)
        center_y = predicted_bbox[1] + ((predicted_bbox[3] - predicted_bbox[1]) // 2)
        
        dict_predicted_results[id] = dict()
        dict_predicted_results[id]['predicted_bbox'] = predicted_bbox
        dict_predicted_results[id]['predicted_class'] = predicted_class + "_" + str(dict_counter_fiducial_class[predicted_class])
        dict_predicted_results[id]['predicted_prob'] = predicted_prob
        dict_predicted_results[id]['predicted_center_coords'] = [center_x, center_y]
        
        start_point = [predicted_bbox[0], predicted_bbox[1]]
        end_point = [predicted_bbox[2]-predicted_bbox[0], predicted_bbox[3]-predicted_bbox[1]]
        temp_image = draw_Rectangle(temp_image, start_point, end_point, color=(0, 255, 0), thickness=1)
        temp_image = draw_Text(temp_image, text=[str(predicted_class) +"-"+ str(predicted_prob)], center_point=(predicted_bbox[0], predicted_bbox[1]-20), fontscale=1, color=(0,255,0), thickness=3)

        dict_counter_fiducial_class[predicted_class] += 1
        
        stdo(1, "[{}] dict_predicted_results:{}".format(
            id,
            dict_predicted_results[id]
        ))
    
    if predicted_class is not None:
        decision = True
    else:
        decision = False
    
    if flag_activate_debug_images:
        image_pack = [temp_image]
        title_pack = [
            str(counter) + "_detected_image_" + str(configuration_id)
        ]
        save_image(image_pack, path="temp_files/detect_Fiducial_Marker/", filename=title_pack, format="png")
        
    stop_time = (time.time() - start_time) * 1000
    result_dashboard = "Fiducial Marker:{} - Pred:[{}] - T:{:.2f}ms".format(decision, str(predicted_class), stop_time)

    return predicted_class, decision, result_dashboard, dict_predicted_results

def detect_Terminal_Component(image, torch_version=str(torch.__version__), process_methods=None, configuration_id='N/A', pattern_id='0', counter=0, flag_activate_debug_images=False):
    
    start_time = time.time()
    pred_size_decision = 0
    decision = False
    predicted_class = None
    predicted_prob = 0
    result_dashboard = "Detection:{} - Pred:[N/A] - T:{}ms".format(decision, 0)
    
    if int(torch_version.split(".")[0]) < 2:
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_resize = cv2.resize(image_rgb, (224, 224)).astype(np.float32) / 255.0
        image_reshape = np.transpose(image_resize, (2, 0, 1))
        image_torch = torch.from_numpy(image_reshape).unsqueeze(0)
        
    else:
        # image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # image_resize = cv2.resize(image_rgb, (224, 224)).astype(np.float32) / 255.0
        # image_reshape = np.transpose(image_resize, (2, 0, 1))
        # image_torch = torch.from_numpy(image_reshape).unsqueeze(0)
        # print("else:::::HERE::::",  image_torch.shape)
        image_torch = image
    
    pred = process_methods.predict(source=image_torch, verbose=False)[0]
    
    list_predicted_class = list()
    for id, cls in enumerate(pred.boxes.cls):
        class_names = pred.names
        predicted_class = class_names[int(cls)]
        predicted_prob = np.round(float(pred.boxes.conf[id]), 2)
        predicted_bbox = list(map(int, pred.boxes.xyxy[id].tolist()))
        
        if int(torch_version.split(".")[0]) < 2:
            predicted_bbox = np.array(predicted_bbox)
            predicted_bbox = predicted_bbox.reshape(-1,2)
            predicted_bbox[:,0], predicted_bbox[:,1] = coordinate_Scaling(
                x=predicted_bbox[:,0], y=predicted_bbox[:,1],
                old_w=224, old_h=224,
                new_w=image.shape[1], new_h=image.shape[0],
                task='RESIZE',
                is_dual=False
            )
            predicted_bbox = predicted_bbox.reshape(-1)
    
        ## The model predicted wrong decision in some images. So bellow codes  ##
        if str(predicted_class) == 'blank':
            if (
                (predicted_bbox[2]-predicted_bbox[0]) > image.shape[1] // 2
            ) or (
                (predicted_bbox[3]-predicted_bbox[1]) > image.shape[0] // 2
            ):
                
                print("::::::Shape-Mismatch:::::", predicted_bbox, " | ", image.shape)
                
                decision = False
            else:
                
                print("::::::Real-Blank:::::", predicted_bbox, " | ", image.shape)
                
                decision = False
        elif str(predicted_class) == 'terminal':
            
            print("::::::Real-Terminal:::::", predicted_bbox, " | ", image.shape)
            
            decision = True
            
        list_predicted_class.append(decision)
    """  
    list_predicted_class.append(decision)  
    list_predicted_class = np.array(list_predicted_class)
    list_index = np.where(list_predicted_class==False)
    matched_list_index = len(list_index)
    if matched_list_index > 0:
        decision = False
    else:
        decision = True
    """
    
    if flag_activate_debug_images:
        image_pack = [image]
        title_pack = [
            str(counter) + "max_matched_frame" + configuration_id
        ]
        save_image(image_pack, path="temp_files/detect_Terminal_Component/", filename=title_pack, format="png")
        
    stop_time = (time.time() - start_time) * 1000
    
    result_dashboard = "[{}][{}] Terminal Detection:{} - Pred:[{}] - T:{:.2f}ms".format(pattern_id, configuration_id, predicted_class, predicted_prob, stop_time)

    return predicted_prob, decision, result_dashboard