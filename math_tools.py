import math
import numpy as np
from random import seed, randint, sample
from scipy.signal import find_peaks
from shapely.geometry import LineString, Point
from sympy import Point, Line
import cv2

from stdo import stdo

def random_Bulk_Data(seed_number, start_range, end_range, number_of_data):
    seed(seed_number)
    return [
        randint(start_range, end_range)
        for i in range(0, number_of_data)
    ]

def random_Bulk_Data_Faster(seed_number, start_range, end_range, number_of_data):
    seed(seed_number)
    return sample(range(start_range, end_range), number_of_data)

def coordinate_Scaling(x, y, old_w, old_h, new_w=0, new_h=0, crop_x=0, crop_y=0, degree=0, task="RESIZE", is_dual=True):
    new_x, new_y = None, None
    if task == "90DEGREE_ROTATION":
        if degree == -90:
            new_x = y
            new_y = new_h - x
        elif degree == 90:
            new_y = x
            new_x = new_w - y

    elif task == "ANGULAR_ROTATION":
        if is_dual == False:
            radian = math.radians(-degree)
            (cx, cy) = (round(new_w/2), round(new_h/2))
            new_x = []
            new_y = []
            for i in range(len(x)):
                v = (x[i]-cx, y[i]-cy)
                teg_x = v[0]*math.cos(radian) - v[1]*math.sin(radian)
                teg_y = v[0]*math.sin(radian) + v[1]*math.cos(radian)
                new_x.append(round(cx + teg_x))
                new_y.append(round(cy + teg_y))
            return np.array(new_x), np.array(new_y)

        else:
            radian = math.radians(-degree)
            (cx, cy) = (round(new_w/2), round(new_h/2))
            v = (x-cx, y-cy)
            new_x = v[0]*math.cos(radian) - v[1]*math.sin(radian)
            new_y = v[0]*math.sin(radian) + v[1]*math.cos(radian)
            new_x = round(cx + new_x)
            new_y = round(cy + new_y)

    elif task == "CROP":
        if is_dual == False:

            # print("x,y:", x, y)

            if (old_w > new_w) and (old_h > new_h):
                new_x = []
                new_y = []
                for i in range(len(x)):
                    new_x.append(x[i] - crop_x)
                    new_y.append(y[i] - crop_y)
            else:
                new_x = []
                new_y = []
                for i in range(len(x)):
                    new_x.append(x[i] + crop_x)
                    new_y.append(y[i] + crop_y)
            # print("new_x, new_y:", new_x, new_y)
            return np.array(new_x), np.array(new_y)

        else:
            # print("x,y:", x, y)
            if (old_w > new_w) and (old_h > new_h):
                new_x = x - crop_x
                new_y = y - crop_y
            else:
                new_x = x + crop_x
                new_y = y + crop_y
            # print("new_x, new_y:", new_x, new_y)

    elif task == "RESIZE":
        if is_dual == False:
            new_x = []
            new_y = []
            for i in range(len(x)):
                new_x.append(round((x[i] / old_w) * new_w))
                new_y.append(round((y[i] / old_h) * new_h))
            return np.array(new_x), np.array(new_y)

        else:
            new_x = round((x / old_w) * new_w)
            new_y = round((y / old_h) * new_h)

    elif task == "HORIZONTAL_FLIP":
        if is_dual == False:
            new_x = []
            new_y = []
            for i in range(len(x)):
                new_x.append(round(old_w - x))
                new_y.append(round(y))
            return np.array(new_x), np.array(new_y)

        else:
            new_x = round(old_w - x)
            new_y = round(y)
    
    elif task == "VERTICAL_FLIP":
        if is_dual == False:
            new_x = []
            new_y = []
            for i in range(len(x)):
                new_x.append(round(x))
                new_y.append(round(old_h - y))
            return np.array(new_x), np.array(new_y)

        else:
            new_x = round(x)
            new_y = round(old_h - y)
    
    elif task == "PROJECTION_TO_XY_AXIS":
        if is_dual == False:
            new_x = []
            new_y = []
            for i in range(len(x)):
                
                new_x.append(int((x[i] * old_w) // (math.sqrt(math.pow(old_w, 2) + math.pow(old_h, 2)))))
                new_y.append(int((y[i] * old_w) // (math.sqrt(math.pow(old_w, 2) + math.pow(old_h, 2))))) # NOT WORK
                
            return np.array(new_x), np.array(new_y)

        else:
            new_x = int((x * old_w) // (math.sqrt(math.pow(old_w, 2) + math.pow(old_h, 2))))
            new_y = int((y * old_w) // (math.sqrt(math.pow(old_w, 2) + math.pow(old_h, 2))))
    
    return new_x, new_y

def fast_fourier_transform(image_gray):
    return np.fft.fft2(image_gray)

def fast_fourier_transform_shift(image_fft):
    return np.fft.fftshift(image_fft)

def fast_fourier_transform_invert(image_fft):
    return abs(np.fft.ifft2(image_fft))

def complex_array_to_image(image_fft, k):
    return np.array(
        np.maximum(
            0,
            np.minimum(
                np.log(np.abs(image_fft)) * k,
                255
            )
        ),
        dtype=np.uint8
    )

def complex_array_info(array_complex):
    # https://dsp.stackexchange.com/questions/22807/how-can-i-get-maximum-complex-value-from-an-array-of-complex-values524289-in-p
    return {
        "max_real": array_complex.real.max(),  # maximum real part
        "max_imag": array_complex.imag.max(),  # maximum imaginary part
        "max_abs": np.abs(array_complex).max(),  # maximum absolute value

        "min_real": array_complex.real.min(),  # minimum real part
        "min_imag": array_complex.imag.min(),  # minimum imaginary part
        "min_abs": np.abs(array_complex).min(),  # minimum absolute value
    }

def extraction_Pixel_Values(image, start_point=(0,0), end_point=(0,0)):
        x0 = start_point[0]
        y0 = start_point[1]
        x1 = end_point[0]
        y1 = end_point[1]
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)

        if x0 < x1:
            ix = 1
        else:
            ix = -1

        if y0 < y1:
            iy = 1
        else:
            iy = -1
        e = 0
        
        currentColor = list()
        for j in range(0, dx+dy, 1):
            currentColor.append((j, image[y0][x0]))

            e1 = e + dy
            e2 = e - dx
            if abs(e1) < abs(e2):
                x0 += ix
                e = e1
            else:
                y0 += iy
                e = e2
        currentColor = np.array(currentColor)
        return currentColor
    
def find_Peak_Points(image, signal, is_scale_x_axis=False, is_scale_y_axis=False):
    peaks, _ = find_peaks(signal[:,1], height=255//2)
    peak_indices = signal[:,0][peaks]

    if is_scale_x_axis or is_scale_y_axis:
        
        peaks, _ = coordinate_Scaling(
            x=peaks,
            y=signal[:,0],
            old_w=image.shape[1],
            old_h=image.shape[0],
            task="PROJECTION_TO_XY_AXIS",
            is_dual=False
        )

    peak_distances = np.diff(peaks)
    if len(peak_distances) > 0:
        peak_max = np.max(peak_distances)
    else:
        peak_max = 0
    return peak_max, peak_distances, peak_indices

def calculation_Determination(a, b):
    return (a[0] * b[1]) - (a[1] * b[0])

def line_Intersection(line1, line2, method='2'):
    if method == '1':
        for i in range(2):
            for j in range(2):
                if line1[i][j] <= 0:
                    line1[i][j] = 0
                elif line2[i][j] <= 0:
                    line2[i][j] = 0
        
        xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
        ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

        div = calculation_Determination(xdiff, ydiff)
        if div == 0:
            return False, False

        d = (calculation_Determination(*line1), calculation_Determination(*line2))
        x = calculation_Determination(d, xdiff) / div
        y = calculation_Determination(d, ydiff) / div
        
        return round(x), round(y)
    
    elif method == '2':
        line_string_1 = LineString([line1[0], line1[1]])
        line_string_2 = LineString([line2[0], line2[1]])
        
        int_pt = line_string_1.intersection(line_string_2)
        if int_pt.is_empty:
            return False, False
        else:
            return round(int_pt.x), round(int_pt.y)
    
    elif method == '3':
        # x1 = line1[0][0], y1 = line1[0][1] 
        # x2 = line1[1][0], y2 = line1[1][1]
        # x3 = line2[0][0], y3 = line2[0][1]
        # x4 = line2[1][0], y4 = line2[1][1]
        
        
        """
        px = ( 
                (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) 
            ) / ( 
                (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) 
            )
        
         
        py = ( 
                (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) 
            ) / ( 
                (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) 
            )
        """
        
        
        px = ( 
              (line1[0][0]*line1[1][1]-line1[0][1] * line1[1][0])*(line2[0][0]-line2[1][0])-(line1[0][0]- line1[1][0])*(line2[0][0]*line2[1][1]-line2[0][1]*line2[1][0]) 
            ) / ( 
                (line1[0][0]- line1[1][0])*(line2[0][1]-line2[1][1])-(line1[0][1] -line1[1][1])*(line2[0][0]-line2[1][0]) 
            ) 
        py = ( 
              (line1[0][0]*line1[1][1]-line1[0][1] * line1[1][0])*(line2[0][1]-line2[1][1])-(line1[0][1] -line1[1][1])*(line2[0][0]*line2[1][1]-line2[0][1]*line2[1][0]) 
            ) / ( 
                 (line1[0][0]- line1[1][0])*(line2[0][1]-line2[1][1])-(line1[0][1] -line1[1][1])*(line2[0][0]-line2[1][0]) 
            )
        
        # stdo(1, "line_Intersection-method:{} center:({}, {})".format(method, px, py))
        
        if (px == float("nan")) or (py == float("nan")):
            return round(px), round(py)
        else:
            return False, False
        
    elif method == '4':
        line_string_1 = Line( Point(line1[0]), Point(line1[1]) )
        line_string_2 = Line( Point(line2[0]), Point(line2[1]) )
        
        is_parallel = line_string_1.is_parallel(line_string_2)
        if is_parallel:
            return False, False
        else:
            is_intersection = line_string_1.intersection(line_string_2)
            cross_coords = np.array(is_intersection).astype(np.int32)[0]
            
            return cross_coords[0], cross_coords[1]

def get_Optimal_Font_Scale(text, width, height):
    for scale in reversed(range(0, 60, 1)):
        (label_width, label_height), baseline = cv2.getTextSize(text, fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=scale/10, thickness=1)
        
        new_width = label_width
        new_height = label_height
        if (new_width <= width) and (new_height <= height):
            return scale/15
    return 1